import {EventEmitter} from 'events';
import {nodelist} from '../storage/memory';
import axios from 'axios';
import {CONFIG} from '../config';

export const contextEvents = new EventEmitter();

contextEvents.on('joining', (_list: string[]) => {
  nodelist.joining = [...nodelist.joining, ..._list];

  // removing duplicates
  nodelist.joining = [...new Set(nodelist.joining)];

  nodelist.joined = nodelist.joined.filter(el => !_list.includes(el));
  nodelist.active = nodelist.active.filter(el => !_list.includes(el));

  contextEvents.emit('nodelist_changed');
});
contextEvents.on('joined', (_list: string[]) => {
  nodelist.joined = [...nodelist.joined, ..._list];

  // removing duplicates
  nodelist.joined = [...new Set(nodelist.joined)];

  nodelist.joining = nodelist.joining.filter(el => !_list.includes(el));
  nodelist.active = nodelist.active.filter(el => !_list.includes(el));

  contextEvents.emit('nodelist_changed');
});

contextEvents.on('active', (_list: string[]) => {
  nodelist.active = [...nodelist.active, ..._list];

  // removing duplicates
  nodelist.active = [...new Set(nodelist.active)];

  nodelist.joining = nodelist.joining.filter(el => !_list.includes(el));
  nodelist.joined = nodelist.joined.filter(el => !_list.includes(el));

  contextEvents.emit('nodelist_changed');
});

export interface NodeIpInfo {
  externalIp: string;
  externalPort: number;
  internalIp: string;
  internalPort: number;
}
contextEvents.on('nodelist_changed', () => {
  // no async, we're testing load
  try {
    for (const node of nodelist.active) {
      axios.post(CONFIG.monitorUrl + `/api/active?ipport=${node}`, {
        nodeId: node,
      });
    }
    for (const node of nodelist.joining) {
      const payload = {
        publicKey: node,
        nodeIpInfo: {
          externalIp: '127.0.0.1',
          externalPort: 9001,
          internalIp: '127.0.0.1',
          internalPort: 9001,
        },
      };
      axios.post(CONFIG.monitorUrl + `/api/joining?ipport=${node}`, payload);
    }
    for (const node of nodelist.joined) {
      const payload = {
        nodeId: node,
        publicKey: node,
        nodeIpInfo: {
          externalIp: '127.0.0.1',
          externalPort: 9001,
          internalIp: '127.0.0.1',
          internalPort: 9001,
        },
      };
      axios.post(CONFIG.monitorUrl + `/api/joined?ipport=${node}`, payload);
    }
  } catch (e: any) {
    console.error(e);
  }
});
