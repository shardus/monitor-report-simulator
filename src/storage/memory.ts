import {setInterval} from 'timers';

interface ShardusNodeList {
  active: string[] | [];
  joined: string[] | [];
  joining: string[] | [];
}

export const nodelist: ShardusNodeList = {
  active: [],
  joined: [],
  joining: [],
};

export let cycle_number = 0;

setInterval(() => {
  cycle_number += 1;
  console.log(cycle_number);
}, 1000 * 60 /* 1min */);
