import {CONFIG} from '../config';
import {cycle_number} from '../storage/memory';

const express = require('express');
const consensorEndpointEmulator = express();
consensorEndpointEmulator.get('/sync-newest-cycle', (req: any, res: any) => {
  console.log('/s');
  res.json(newestCycle);
});

consensorEndpointEmulator.get('/config', (req: any, res: any) => {
  console.log('/c');
  res.json(config);
});

export default consensorEndpointEmulator;

const config = {
  config: {
    heartbeatInterval: CONFIG.hearbeatInterval,
    baseDir: '.',
    transactionExpireTime: 5,
    crypto: {
      hashKey:
        '69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc',
    },
    p2p: {
      ipServer: 'api.ipify.org/?format=json',
      timeServers: [
        '0.pool.ntp.org',
        '1.pool.ntp.org',
        '2.pool.ntp.org',
        '3.pool.ntp.org',
      ],
      existingArchivers: [
        {
          ip: 'localhost',
          port: 4000,
          publicKey:
            '758b1c119412298802cd28dbfa394cdfeecc4074492d60844cc192d632d84de3',
        },
      ],
      syncLimit: 180,
      cycleDuration: 60,
      maxRejoinTime: 20,
      difficulty: 2,
      queryDelay: 1,
      gossipRecipients: 8,
      gossipFactor: 4,
      gossipStartSeed: 15,
      gossipSeedFallof: 15,
      gossipTimeout: 180,
      maxSeedNodes: 10,
      minNodesToAllowTxs: 1,
      minNodes: 50,
      maxNodes: 500,
      seedNodeOffset: 4,
      nodeExpiryAge: 30,
      maxJoinedPerCycle: 4,
      maxSyncingPerCycle: 8,
      syncBoostEnabled: false,
      maxSyncTimeFloor: 18000,
      maxNodeForSyncTime: 9,
      maxRotatedPerCycle: 1,
      firstCycleJoin: 0,
      maxPercentOfDelta: 40,
      minScaleReqsNeeded: 5,
      maxScaleReqs: 200,
      scaleConsensusRequired: 0.25,
      amountToGrow: 10,
      amountToShrink: 5,
      maxDesiredMultiplier: 1.2,
      startInWitnessMode: false,
      experimentalSnapshot: true,
      detectLostSyncing: true,
    },
    ip: {
      externalIp: '127.0.0.1',
      externalPort: 9001,
      internalIp: '127.0.0.1',
      internalPort: 10001,
    },
    network: {timeout: 5},
    reporting: {
      report: true,
      recipient: 'http://localhost:3000/api',
      interval: 2,
      console: false,
    },
    debug: {
      loseReceiptChance: 0,
      loseTxChance: 0,
      canDataRepair: false,
      startInFatalsLogMode: false,
      startInErrorLogMode: true,
      fakeNetworkDelay: 0,
      disableSnapshots: true,
      disableTxCoverageReport: true,
      haltOnDataOOS: false,
      countEndpointStart: -1,
      countEndpointStop: -1,
      hashedDevAuth:
        '117151a0d719f329a4a570b4ccde4bbb1ec12fa6a7a870a1471ed170e7f08d70',
      devPublicKey:
        '774491f80f47fedb119bb861601490f42bc3ea3b57fc63906c0d08e6d777a592',
      newCacheFlow: true,
      debugNoTxVoting: false,
      ignoreRecieptChance: 0,
      ignoreVoteChance: 0,
      failReceiptChance: 0,
      voteFlipChance: 0,
      skipPatcherRepair: false,
      failNoRepairTxChance: 0,
      useNewParitionReport: false,
      oldPartitionSystem: false,
      dumpAccountReportFromSQL: false,
      profiler: false,
      optimizedTXConsenus: true,
      robustQueryDebug: true,
      forwardTXToSyncingNeighbors: false,
    },
    statistics: {save: true, interval: 1},
    loadDetection: {
      queueLimit: 200,
      desiredTxTime: 15,
      highThreshold: 0.5,
      lowThreshold: 0.2,
    },
    rateLimiting: {
      limitRate: true,
      loadLimit: {
        internal: 0.8,
        external: 0.8,
        txTimeInQueue: 0.7,
        queueLength: 0.8,
      },
    },
    stateManager: {
      stateTableBucketSize: 500,
      accountBucketSize: 200,
      patcherAccountsPerRequest: 250,
      patcherAccountsPerUpdate: 2500,
      patcherMaxHashesPerRequest: 300,
      patcherMaxLeafHashesPerRequest: 300,
      patcherMaxChildHashResponses: 2000,
      maxDataSyncRestarts: 5,
      maxTrackerRestarts: 5,
    },
    sharding: {nodesPerConsensusGroup: 5, executeInOneShard: true},
    mode: 'debug',
  },
};
const newestCycle = {
  newestCycle: {
    networkId:
      '480143f2399d0081f136cd3193ee0809334be64ee3d1eb3c985960ebd62d4850',
    counter: cycle_number,
    previous:
      'f25a8aa3f2755e0c407fd4fd50f9f4766f113de706efdf1df7837435fbf135ae',
    start: 1664798113,
    duration: 60,
    joined: [],
    returned: [],
    lost: [],
    lostSyncing: [],
    refuted: [],
    apoptosized: [],
    joinedArchivers: [],
    leavingArchivers: [],
    syncing: 1,
    joinedConsensors: [],
    active: 0,
    activated: [],
    activatedPublicKeys: [],
    maxSyncTime: 18000,
    expired: 0,
    removed: [],
    refreshedArchivers: [
      {
        curvePk:
          '363afebb8cca474bd4e3c29d0109ad068736b7802c34ed8b7038cd6a95bb1e24',
        ip: 'localhost',
        port: 4000,
        publicKey:
          '758b1c119412298802cd28dbfa394cdfeecc4074492d60844cc192d632d84de3',
      },
    ],
    refreshedConsensors: [],
    safetyMode: false,
    safetyNum: 0,
    networkStateHash: '',
    networkDataHash: [],
    networkReceiptHash: [],
    networkSummaryHash: [],
    desired: 50,
  },
};
