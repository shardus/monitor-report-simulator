import {nodelist} from './storage/memory';
import * as crypto from '@shardus/crypto-utils';
import {contextEvents} from './events/';
import {dummyHeartbeat, getRandomInt, sleep} from './utils';
import axios from 'axios';
import {CONFIG} from './config';
import consensorEndpointEmulator from './consensor/consensorEndpointEmulator';

crypto.init('64f152869ca2d473e4ba64ab53f49ccdb2edae22da192c126850970e788af347');

consensorEndpointEmulator.listen(9001, () => {
  console.log('consensor endpoint simulator on port: 9001');
});

async function main() {
  const list: any[] = [];
  for (let i = 0; i < 100; i++) {
    list.push(`127.0.0.${getRandomInt(255)}_${900 + i}`);
  }

  contextEvents.emit('joining', [list[0], list[1], list[3]]);
  contextEvents.emit('joined', [list[4], list[5], list[6]]);
  contextEvents.emit('active', [list[7], list[8], list[9]]);

  setInterval(() => {
    for (const node of nodelist.active) {
      const payload = {
        nodeId: node,
        data: dummyHeartbeat(node),
      };
      axios.post(CONFIG.monitorUrl + `/api/heartbeat?ipport=${node}`, payload);
    }
  }, 1000 * CONFIG.hearbeatInterval);
}

process.on('uncaughtException', err => {
  console.log('uncaughtException:' + err);
});
process.on('unhandledRejection', err => {
  console.log('unhandledRejection:' + err);
});
main();
