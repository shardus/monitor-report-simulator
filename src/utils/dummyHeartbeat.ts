import * as crypto from '@shardus/crypto-utils';
import {CONFIG} from '../config';
import {cycle_number, nodelist} from '../storage/memory';

export default (_nodeId: string) => {
  const _txRejected = Math.floor(Math.random() * 2);
  const _txExpired = Math.floor(Math.random() * 2);
  const _txProcessed = Math.floor(Math.random() * CONFIG.hearbeatInterval * 15);
  const _txInjected = _txProcessed + _txRejected + _txExpired;
  const data = {
    repairsStarted: 0,
    repairsFinished: 0,
    isDataSynced: true,
    appState: crypto.randomBytes(64),
    cycleMarker: crypto.hashObj({nodelist: nodelist.active, cycle_number}),
    cycleCounter: cycle_number,
    nodelistHash: crypto.hashObj(nodelist.active),
    desiredNodes: 50,
    lastScalingTypeWinner: null,
    lastScalingTypeRequested: null,
    txInjected: _txInjected,
    txApplied: _txProcessed,
    txRejected: _txRejected,
    txExpired: _txExpired,
    txProcessed: _txProcessed,
    reportInterval: 1000 * CONFIG.hearbeatInterval,
    nodeIpInfo: {
      externalIp: '127.0.0.1',
      externalPort: 9001,
      internalIp: '127.0.0.1',
      internalPort: 9001,
    },
    partitionReport: {},
    globalSync: true,
    partitions: 1,
    partitionsCovered: 1,
    currentLoad: {networkLoad: 0, nodeLoad: [Object]},
    queueLength: 0,
    txTimeInQueue: 0,
    rareCounters: {},
    txCoverage: {},
    isLost: false,
    isRefuted: false,
    shardusVersion: '2.6.30',
  };
  return data;
};
